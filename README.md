# Simple Todo List
Key words: react, redux, saga, axios, firebase
Project use react, react-redux, redux, redux-saga, redux-thunk, and axios etc. Very good sample of usage of redux and saga together to move the side-effects to saga, so the action creator is very clean. Use the authentication and database services from Firebase for backend, authentication service returns token when making the sign in/up restful API calls. Using the token from authentication service can protest the resource from database service. 

## Getting Started

### Installing

1. Clone the project
```
git clone git@bitbucket.org:waynelei/to-do.git

or

git clone https://waynelei@bitbucket.org/waynelei/to-do.git

```

2. Install dependencies 
```
cd todo
npm install
```

## Running
```
npm start
```

## Demo
[Here](http://todo.hoyee.com)


## Authors
* **Wayne Lei** -  [WayneLei](https://bitbucket.org/waynelei)