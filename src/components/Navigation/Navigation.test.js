import React from 'react';

import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { NavLink } from 'react-router-dom';

import Navigation from './Navigation';


configure({ adapter: new Adapter() });

describe('<Navigation />', () => {
    it('should show logout if authenticated', () => {
        const wrapper = shallow(<Navigation />);
        // console.log(wrapper.debug());
        // expect(wrapper.exists('Logout')).to.equal(true);
        // expect(wrapper.text()).to.equal('Logout');
        // expect(wrapper.contains(<nav />)).toEqual(true);
        // expect(
        //     wrapper.containsMatchingElement(
        //       <span>abc</span>
        //     )
        //   ).toBeTruthy()

        // const wrapper = render(<Navigation />);
        expect(wrapper.find('NavLink')).toHaveLength(3);
        expect(wrapper.text()).toContain('NavLink');

        const li = wrapper.find('li');
        expect(li).toHaveLength(3);
        // console.log(li.debug());

        // const li2 = li[2];
        // console.log(li2.debug());


    });
});