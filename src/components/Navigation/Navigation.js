import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './Navigation.css';

class Navigation extends Component {

    clickHandler(e, num) {
        if (!this.props.isAuth && [0, 1].includes(num)) {
            e.preventDefault();
            return
        }
    }

    render() {
        const style = this.props.isAuth ? 'nav-link' : 'nav-link disabled';
        return (
            
            <nav className="navbar fixed-top navbar-light bg-light">
                <ul className="nav nav-tabs nav-fill w-100 Navigation" >
                    <li className="nav-item">
                        <NavLink className={style} to='/todo' onClick={(e) => this.clickHandler(e, 0)}>Todos</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className={style} to="/add" onClick={(e) => this.clickHandler(e, 1)}>Add Todo</NavLink>
                    </li>
                    <li className="nav-item">
                        {
                            this.props.isAuth
                                ? <NavLink className='nav-link' to="/logout" onClick={(e) => this.clickHandler(e, 2)}>Logout</NavLink>
                                : <NavLink className='nav-link' to="/sign" onClick={(e) => this.clickHandler(e, 2)}>Sign In/Up</NavLink>
                        }
                    </li>
                </ul>
            </nav>
        )
    }

}

export default Navigation;
