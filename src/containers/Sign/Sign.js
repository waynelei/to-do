import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';

import { FormErrors } from '../../components/Utility/FormErrors/FormErrors';
import './Sign.css';

export class Sign extends Component {
    state = {
        email: '',
        password: '',
        formErrors: { email: '', password: '' },
        emailValid: false,
        passwordValid: false,
        formValid: false,
        isSignUp: false
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;


        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short';
                break;

            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }



    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    submitHandler = (event) => {
        event.preventDefault();
        const payload = {
            email: this.state.email,
            password: this.state.password,
            isSignUp: this.state.isSignUp
        }
        this.props.sign(payload, this.props.history.replace);
    }


    setSignUp() {
        this.setState(prevState => {
            return {
                ...this.state,
                isSignUp: !prevState.isSignUp
            }
        })
    }

    render() {
        return (
            <div className='Sign'>

                <h2>
                    {
                        this.state.isSignUp
                            ? 'Sign up'
                            : 'Sign In'
                    }
                </h2>
                <div className="panel panel-default">
                    <FormErrors formErrors={this.state.formErrors} />
                </div>

                <form className='demoForm' onSubmit={this.submitHandler}>

                    <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                        <label htmlFor="email">Email address</label>
                        <input type="email" required className="form-control width100 input" name="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.handleUserInput} />
                    </div>

                    <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control width100" name="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleUserInput} />
                    </div>
                    <button type="submit" className="btn btn-primary button100" disabled={!this.state.formValid}>
                        {
                            this.state.isSignUp
                                ? 'Sign Up'
                                : 'Sign In'
                        }
                    </button>

                </form>
                <hr />
                <button onClick={() => this.setSignUp()}>
                    {
                        this.state.isSignUp
                            ? 'Already Sign up? Sign In Here'
                            : 'Not Sign Up Yet? Sign Up Here'
                    }

                </button>
                <div>
                    <br />
                </div>
                <br />
            </div>
        )
    }
}



export default connect(null, actions)(Sign);