import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Sign } from './Sign';

configure({ adapter: new Adapter() });

describe('<Sign />', () => {
    it('should change to sign up from sign in', ()=> {


        const wrapper = shallow(<Sign />);
        let button = wrapper.find('button').last();
        let text = button.text();
        console.log(button.debug());
        expect(text).toEqual('Not Sign Up Yet? Sign Up Here');
        // console.log(text)
        button.simulate('click');
        button = wrapper.find('button').at(1);
        text = button.text();
        console.log(button.debug());
        expect(text).toEqual('Already Sign up? Sign In Here');
        // console.log(text)
        
        

    })
})