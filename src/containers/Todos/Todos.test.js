import React from 'react';

import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Todos } from './Todos';
import Todo from './Todo/Todo';


configure({ adapter: new Adapter() });

describe("", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<Todos get_todos={ ()=> {} } todos={[]}/>)
    });

    it("should have no Todo element", () => {
        // console.log("33424eafdfasfasdfasdfsa")
        // const wrapper = shallow(<Todos get_todos={ ()=> {} } todos={[todos]}/>);
        // wrapper.setProps({ todos: []});
        // console.log(wrapper.debug())
        expect(wrapper.find(Todo)).toHaveLength(0);
    });


    it("should have 2 Todo elements", () => {
        let todos = [
            {
                description: "AFDSFASFSADFSADFASCXZ",
                id: "-LWcjTh82ZxuyV1qXLz8",
                reverseTime: -1547945503454,
                status: "In progress",
                timeCreated: "2019-01-20T00:51:43.454Z",
                title: "aaaaaa",
                userId: "fHgegWFBPSVBUuGXOWr3t1Ta8Yg1"
            },
            {
                description: "22222222222222222222",
                id: "-LW23eTh82ZxuyV1qXLz8",
                reverseTime: -1547945503454,
                status: "In progress",
                timeCreated: "2019-01-20T00:51:43.454Z",
                title: "aaaaaa",
                userId: "fHgegWFBPSVBUuGXOWr3t1Ta8Yg1"
            }
        ]
        // const wrapper = shallow(<Todos />);
        // wrapper = shallow(<Todos get_todos={ ()=> {} } todos={[todos]}/>);
        wrapper.setProps({ todos: todos });
        // console.log(wrapper.debug())
        expect(wrapper.find(Todo)).toHaveLength(2);
    });

    it("should have {clickId: null} state", () => {
        const stateClickId = wrapper.state().clickId;
        expect(stateClickId).toEqual(null);
    });
})