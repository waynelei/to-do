import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';

import Todo from './Todo/Todo';
import './Todos.css';

export class Todos extends Component {

    state = {
        clickId : null
    }

    onChangeStatusHandler(todo) {
        this.props.change_todo_status({...todo}, this.props.token);
    }

    onDeleteHandler(todo) {
        this.props.delete_todo({...todo}, this.props.token);
    }

    componentDidMount() {
        // console.log("222");
        if(this.props.token){
            this.props.get_todos(this.props.token, this.props.userId);
        } else {
            setTimeout(()=>{
                this.props.get_todos(this.props.token, this.props.userId);
            }, 500);
        }
    }

    render() {
            // console.log(this.props.todos);
            let todos = 
                this.props.todos.map((todo) => {
                    return (
                        <Todo
                            key={todo.id}
                            title={todo.title}
                            todo={todo.description}
                            userId={todo.userId}
                            status={todo.status}
                            timeCreated={todo.timeCreated}
                            loading={this.props.loading}
                            editTodo={todo}
                            clicked={() => { this.onChangeStatusHandler(todo) }} 
                            onDelete={()=>{ this.onDeleteHandler(todo)}}/>
                    )
                })
        return (
            <div className='Todos' >
                {todos}
            </div>
        )
    }
}

const mapStateToProp = (state) => {
    return {
        todos: state.todo.todos,
        loading: state.todo.loading,
        token: state.user.token,
        userId: state.user.userId
    }
}

export default connect(mapStateToProp, actions)(Todos);