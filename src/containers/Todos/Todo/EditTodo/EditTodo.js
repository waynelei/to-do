import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../../../store/actions/index';

import { FormErrors } from '../../../../components/Utility/FormErrors/FormErrors';
import './EditTodo.css'

class EditTodo extends Component {
    state = {
        description: '',
        title: '',
        formErrors: {
            description: '',
            title: ''
        },
        descriptionValid: true,
        titleValid: true,
        formValid: true,
    }

    componentDidMount() {
        // console.log(this.props.todo)
        this.setState({
            ...this.state,
            title: this.props.todo.title,
            description: this.props.todo.description
        })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let descriptionValid = this.state.descriptionValid;
        let titleValid = this.state.titleValid;


        switch (fieldName) {
            case 'description':
                descriptionValid = value.length >= 20;
                fieldValidationErrors.description = descriptionValid ? '' : ' is too short';
                break;
            case 'title':
                titleValid = value.length >= 6;
                fieldValidationErrors.title = titleValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            descriptionValid: descriptionValid,
            titleValid: titleValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.descriptionValid && this.state.titleValid });
    }



    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }


    submitHandler = (event) => {
        let { token, userId } = this.props
        event.preventDefault();
        // const payload = {
        //     title: this.state.title,
        //     description: this.state.description,
        //     status: 'To do',
        //     userId: this.props.userId,
        //     timeCreated: new Date(),
        //     reverseTime: 0 - Date.now()
        // }
        const payload = {
            ...this.props.todo,
            title: this.state.title,
            description: this.state.description,
        }

        this.props.edit_todo(payload, this.props.token);
        this.props.onClose();
        setTimeout(() => this.props.get_todos(token, userId), 399);
    }

    render() {
        return (
            <div className='AddTodo'>
                <form onSubmit={this.submitHandler}>
                    <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    <div className="form-group">
                        <label >Title</label>
                        <input
                            required
                            type="text"
                            className="form-control"
                            aria-describedby="emailHelp"
                            name="title"
                            placeholder="Title"
                            value={this.state.title}
                            onChange={this.handleUserInput} />
                    </div>
                    <div className="form-group">
                        <label >Description</label>
                        <textarea
                            required
                            className="form-control width100 input"
                            name="description"
                            placeholder="Description"
                            value={this.state.description}
                            onChange={this.handleUserInput} />
                    </div>
                    <div className="btn-toolbar">
                        <div className="btn-group mr-2">
                            <button type="submit" disabled={!this.state.formValid} className="btn btn-outline-primary">Update</button>
                        </div>
                        <div className="btn-group mr-2">
                            <button
                                type="button"
                                className="btn btn-outline-info"
                                onClick={this.props.onClose}>Cancle</button>
                        </div>
                    </div>

                </form>

            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        userId: state.user.userId,
        token: state.user.token
    }
}

export default withRouter(connect(mapStateToProps, actions)(EditTodo));