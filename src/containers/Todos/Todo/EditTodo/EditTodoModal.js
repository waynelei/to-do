import React, { Component } from 'react';
import Modal from 'react-modal';

import Edit from './EditTodo';
import './EditTodo.css';

const customStyles = {
    content: {
        top: '10%',
        left: '2%',
        right: '2%',
        bottom: '10%'
    }
};

Modal.setAppElement('body')

class EditTodoModal extends Component {

    render() {
        return (
            <div className='EditTodo'>
                <Modal
                    isOpen={this.props.isOpen}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <Edit 
                    onClose={()=>this.props.openEdit(false)}
                    todo={this.props.todo}/>
                </Modal>
            </div>
        );
    }
}

export default EditTodoModal;