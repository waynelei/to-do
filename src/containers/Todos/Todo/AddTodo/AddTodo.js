import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../../store/actions/index';

import { FormErrors } from '../../../../components/Utility/FormErrors/FormErrors';
import './AddTodo.css'
import '../../../Form/Form.css'

class AddTodo extends Component {
    state = {
        description: '',
        title: '',
        formErrors: {
            description: '',
            title: ''
        },
        descriptionValid: false,
        titleValid: false,
        formValid: false,
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let descriptionValid = this.state.descriptionValid;
        let titleValid = this.state.titleValid;


        switch (fieldName) {
            case 'description':
                descriptionValid = value.length >= 20;
                fieldValidationErrors.description = descriptionValid ? '' : ' is too short';
                break;
            case 'title':
                titleValid = value.length >= 6;
                fieldValidationErrors.title = titleValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            descriptionValid: descriptionValid,
            titleValid: titleValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.descriptionValid });
    }



    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }


    submitHandler = (event) => {
        event.preventDefault();
        const payload = {
            title: this.state.title,
            description: this.state.description,
            status: 'To do',
            userId: this.props.userId,
            timeCreated: new Date(),
            reverseTime: 0 - Date.now()
        }

        this.props.add_todo(payload, this.props.token, this.props.history.replace);
    }

    render() {
        return (
            <div className='AddTodo'>
                <form onSubmit={this.submitHandler}>
                    <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    <div className="form-group">
                        <label >Title</label>
                        <input
                            required
                            type="text"
                            className="form-control width100 input"
                            aria-describedby="emailHelp"
                            name="title"
                            placeholder="Title"
                            value={this.state.title}
                            onChange={this.handleUserInput} />
                    </div>
                    <div className="form-group">
                        <label >Description</label>
                        <textarea
                            required
                            className="form-control width100 input"
                            name="description"
                            placeholder="Description"
                            value={this.state.description}
                            onChange={this.handleUserInput} />
                    </div>
                    <button type="submit" disabled={!this.state.formValid} className="btn btn-primary">Add</button>
                </form>

            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        userId: state.user.userId,
        token: state.user.token
    }
}


const mapDispatchToProps = dispatch => {
    return {
        add_todo: (payload, token, replace) => dispatch(actions.add_todo(payload, token, replace)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);