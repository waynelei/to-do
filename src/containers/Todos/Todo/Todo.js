import React, { Component } from 'react';
import './Todo.css'
import EditTodoModal from './EditTodo/EditTodoModal';

class Todo extends Component {
    state = {
        isEditOpen: false
    }

    openEdit(isOpen) {
        this.setState({
            ...this.state,
            isEditOpen: isOpen
        });
    }

    onEditHandle(e) {
        e.stopPropagation();
        this.openEdit(true);
    }

    deleteTodo(e, todo) {
        e.stopPropagation();
        this.props.onDelete(todo);
    }

    render() {

        let status = 'Todo-';
        switch (this.props.status) {
            case 'In progress':
                status = 'Todo-Progress';
                break;
            case 'Done':
                status = 'Todo-Done';
                break;
            case 'To do':
                status = 'Todo-Todo';
                break;
            default:
                status = ''
        }
        return (
            <div className={`card shadow-lg Todo ${status}`}>
                <div className="card-body" onClick={(e) => { this.onEditHandle(e) }}>
                    <h5 className="card-title">{this.props.title}</h5>
                    <p className="card-text Todo-Description" >{this.props.todo}</p>
                    <div className="btn-group mr-2">
                        <button
                            className="btn btn-primary btn-sm"
                            onClick={(e) => {
                                e.stopPropagation();
                                this.props.clicked();
                            }} >{this.props.status}</button>
                    </div>
                    <div className="btn-group mr-2">
                        <button
                            className="btn btn-danger btn-sm"
                            onClick={(e, todo) => {
                                e.stopPropagation();
                                if (window.confirm('Are you sure you wish to delete this item?')) this.deleteTodo(e, this.props.editTodo)
                            }} >DELETE</button>
                    </div>

                    {/* <div className='delete-button' onClick={() => { if (window.confirm('Are you sure you wish to delete this item?')) this.onCancel(item) } } /> */}

                </div>
                <EditTodoModal
                    isOpen={this.state.isEditOpen}
                    openEdit={(isOpen) => this.openEdit(isOpen)}
                    todo={this.props.editTodo} />
            </div>

        )
    }
}

export default Todo;