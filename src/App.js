import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from './store/actions/index';


import './App.css';
import Navigation from './components/Navigation/Navigation';
import Sign from './containers/Sign/Sign';
import Todos from './containers/Todos/Todos';
import AddTodo from './containers/Todos/Todo/AddTodo/AddTodo';
import Logout from './containers/Sign/Logout/Logout';


class App extends Component {

  componentDidMount() {
    this.props.check_local_store_state();
  }

  render() {

    let routes = (
      <Switch>
        <Route path='/todo' component={Todos} />
        <Route path='/add' component={AddTodo} />
        <Route path='/logout' component={Logout} />
        <Redirect to="/todo" />
      </Switch>
    );

    if (!this.props.isAuth) {
      routes = (
        <Switch>
          <Route path="/sign" component={Sign} />
          <Redirect to="/sign" />
        </Switch>
      );
    }
    return (
      <div className="App">
        <Navigation isAuth={this.props.isAuth} />
        {routes}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.user.token !== null
  }
}
export default withRouter(connect(mapStateToProps, actions)(App));
