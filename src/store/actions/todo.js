import * as actionTypes from './actionTypes';


export const add_todo = (payload, token, replace) => {
    return {
        type: actionTypes.ADD_TODO,
        payload: payload,
        token,
        replace
    }
};

export const edit_todo = (todo, token) => {
    return {
        type: actionTypes.EDIT_TODO,
        todo: todo,
        token
    }
};

export const delete_todo = (todo, token) => {
    return {
        type: actionTypes.DELETE_TODO,
        todo: todo,
        token
    }
};

export const change_todo_status = (todo, token) => {
    return {
        type: actionTypes.CHANGE_TODO_STATUS,
        todo: todo,
        token
    }
}

export const get_todos = (token, userId) => {
    return {
        type: actionTypes.GET_TODOS,
        token,
        userId
    }
}

export const set_todos = (todos) => {
    return {
        type: actionTypes.SET_TODOS,
        todos: todos
    }
}


export const set_todo_loading = (loading) => {
    return {
        type: actionTypes.SET_TODO_LOADING,
        loading: loading
    }
}
