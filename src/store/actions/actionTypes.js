export const GET_TODOS = 'GET_TODOS';
export const SET_TODOS = 'SET_TODOS';
export const ADD_TODO = 'ADD_TODO';
export const CHANGE_TODO_STATUS = 'CHANGE_TODO_STATUS';
export const SET_TODO_LOADING = 'SET_TODO_LOADING';
export const EDIT_TODO = 'EDIT_TODO';
export const DELETE_TODO = 'DELETE_TODO';


// export const SIGN_IN = 'SIGN_IN';
// export const SIGN_UP = 'SIGN_UP';
export const SIGN = 'SIGN';
export const SET_TOKEN = 'SET_TOKEN';
export const LOGOUT = 'LOGOUT';
export const IS_AUTH = 'IS_AUTH';
export const CHECK_TIMEOUT = 'CHECK_TIMEOUT';
export const CHECK_LOCAL_STORE_STATE = 'CHECK_LOCAL_STORE_STATE';


