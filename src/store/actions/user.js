import * as actionTypes from './actionTypes';


// export const sign_in = (payload) => {
//     return {
//         type: actionTypes.SIGN_IN,
//         payload: payload
//     }
// };

export const sign = (payload, replace) => {
    return {
        type: actionTypes.SIGN,
        payload: payload,
        replace
    }
};

export const set_token = (token, userId) => {
    return {
        type: actionTypes.SET_TOKEN,
        token: token,
        userId: userId
    }
}

// export const is_auth = () => {
//     return {
//         type: actionTypes.IS_AUTH
//     }
// }

export const logout = () => {
    return {
        type: actionTypes.LOGOUT
    }
}

export const check_timeout = (expirationTime) => {
    return {
        type: actionTypes.CHECK_TIMEOUT,
        expirationTime: expirationTime
    }
}

export const check_local_store_state = () => {
    return {
        type: actionTypes.CHECK_LOCAL_STORE_STATE
    };
};