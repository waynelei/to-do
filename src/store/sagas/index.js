import { takeEvery, all } from "redux-saga/effects";

import * as actionTypes from "../actions/actionTypes";


import {
    addTodoSaga, 
    getTodoSaga,
    changeTodoStatusSaga,
    editTodoSaga,
    deleteTodoSaga
} from './todo';

import {
    signSaga, 
    logoutSaga,
    checkTimeoutSaga,
    checkLocalStoreStateSaga
} from './user';


export function* watchTodo() {
    yield all([
      takeEvery(actionTypes.ADD_TODO, addTodoSaga),
      takeEvery(actionTypes.GET_TODOS, getTodoSaga),
      takeEvery(actionTypes.CHANGE_TODO_STATUS, changeTodoStatusSaga),
      takeEvery(actionTypes.EDIT_TODO, editTodoSaga),
      takeEvery(actionTypes.DELETE_TODO, deleteTodoSaga)
    ]);
}

export function* watchUser() {
    yield all([
      takeEvery(actionTypes.SIGN, signSaga),
      takeEvery(actionTypes.LOGOUT, logoutSaga),
      takeEvery(actionTypes.CHECK_TIMEOUT, checkTimeoutSaga),
      takeEvery(actionTypes.CHECK_LOCAL_STORE_STATE, checkLocalStoreStateSaga)
    ]);
}