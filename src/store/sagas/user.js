import axios from 'axios';
import { put } from "redux-saga/effects";
import { delay } from "redux-saga";

import * as actions from "../actions/index";

const API_KEY = 'AIzaSyDAvsT8GuGHXJgUB-Kibv05RtJTeyng1CM';

export function* signSaga(action) {
    const authData = {
        email: action.payload.email,
        password: action.payload.password,
        returnSecureToken: true
    }
    let url =
        action.payload.isSignUp
            ? 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=' + API_KEY
            : 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=' + API_KEY
    try {
        const resp =
            yield axios.post(
                url,
                authData,
            );
        let { idToken, localId, expiresIn } = resp.data;
        const expirationDate = yield new Date(
            new Date().getTime() + expiresIn * 1000
        );
        yield setLocalStore(idToken, localId, expirationDate)
        yield put(actions.check_timeout(expiresIn));
        if (action.payload.isSignUp) {
            yield prepopulate(idToken, localId, action.replace);
        }
        yield put(actions.set_token(idToken, localId));
        yield action.replace('/todo');

    } catch (error) {
        alert(error.response.data.error.message);
    }
}

export function* checkTimeoutSaga(action) {
    yield delay(action.expirationTime * 1000);
    yield put(actions.logout());
}

export function* logoutSaga(action) {
    yield localStorage.removeItem("token");
    yield localStorage.removeItem("userId");
    yield localStorage.removeItem("expiresDate");
    yield put(actions.set_token(null, null));
}

function* setLocalStore(token, userId, expiresDate) {
    yield localStorage.setItem("token", token);
    yield localStorage.setItem("userId", userId);
    yield localStorage.setItem("expirationDate", expiresDate);
}

export function* checkLocalStoreStateSaga(action) {
    const token = yield localStorage.getItem("token");
    if (!token) {
        yield put(actions.logout());
    } else {
        const expirationDate = yield new Date(
            localStorage.getItem("expirationDate")
        );
        if (expirationDate <= new Date()) {
            yield put(actions.logout());
        } else {
            const userId = yield localStorage.getItem("userId");
            yield put(actions.set_token(token, userId));
            yield put(
                actions.check_timeout(
                    (expirationDate.getTime() - new Date().getTime()) / 1000
                )
            );
        }
    }
}

function* prepopulate(token, userId, replace){
    let payload = {
        title: '1. Sign up/in',
        description: 'Enter your email and password to sign in/up. You will see the prepopulated tasks on Todo tab ',
        status: 'To do',
        userId: userId,
        timeCreated: new Date(),
        reverseTime: 0 - Date.now()
    }
    yield delay(5);
    yield put(actions.add_todo(payload, token));
    payload = {
        title: '2. Add Todo',
        description: 'Enter title and description fields, click on Add button. ',
        status: 'To do',
        userId: userId,
        timeCreated: new Date(),
        reverseTime: 0 - Date.now()
    }
    yield delay(5);
    yield put(actions.add_todo(payload, token));
    payload = {
        title: '3. Change tasks status',
        description: 'On the list of tasks, click on the status button to change the status',
        status: 'To do',
        userId: userId,
        timeCreated: new Date(),
        reverseTime: 0 - Date.now()
    }
    yield put(actions.add_todo(payload, token));    
    yield delay(500);
}