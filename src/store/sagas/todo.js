import axios from 'axios';
import { put } from "redux-saga/effects";
// import { delay } from 'redux-saga'
import * as actions from "../actions/index";

export function* addTodoSaga(action) {

    let data = {
        ...action.payload
    };

    try {
        yield axios.post(
            'https://to-do-56f1c.firebaseio.com/todos.json?auth=' + action.token,
            data,
        );
        if (action.replace !== undefined) {
            action.replace('/todo');
        }
    } catch (error) {
        console.log(error)
    }
}


export function* getTodoSaga(action) {
    yield put(actions.set_todo_loading(true));
    const queryParams =
        "?auth=" +
        action.token +
        '&orderBy="userId"&equalTo="' +
        action.userId +
        '"';
    try {
        const resp = yield axios.get(
            'https://to-do-56f1c.firebaseio.com/todos.json' + queryParams,
        );
        let todos = resp.data;
        if (todos === null) return;
        let todosArray = Object.keys(todos)
            .map(igKey => {
                return { ...todos[igKey], id: igKey }
            })
        todosArray.sort((a, b) => (a.reverseTime) - (b.reverseTime));
        yield put(actions.set_todos(todosArray));

    } catch (error) {
        console.log(error);
    }
}


export function* changeTodoStatusSaga(action) {

    const id = action.todo.id;
    let todo = action.todo;
    delete todo.id;

    const statuses = ['To do', 'In progress', 'Done']
    let index = statuses.findIndex((s) => todo.status === s)
    index = (index + 1) % 3;

    todo.status = statuses[index];

    yield put(actions.set_todo_loading(true));

    try {
        yield axios.patch(
            'https://to-do-56f1c.firebaseio.com/todos/' + id + '.json?auth=' + action.token,
            todo ,
        );

        yield put(actions.get_todos(action.token, action.todo.userId));


    } catch (error) {
        console.log(error)

    }

}



export function* editTodoSaga(action) {

    const id = action.todo.id;
    let todo = action.todo;
    delete todo.id;

    try {
        yield axios.patch(
            'https://to-do-56f1c.firebaseio.com/todos/' + id + '.json?auth=' + action.token,
            todo ,
        );

        yield put(actions.get_todos(action.token, action.todo.userId));


    } catch (error) {
        console.log(error)

    }

}


export function* deleteTodoSaga(action) {

    const id = action.todo.id;
    let todo = action.todo;
    delete todo.id;

    try {
        yield axios.delete(
            'https://to-do-56f1c.firebaseio.com/todos/' + id + '.json?auth=' + action.token,
        );

        yield put(actions.get_todos(action.token, action.todo.userId));


    } catch (error) {
        console.log(error)

    }

}