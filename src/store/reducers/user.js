import * as actionTypes from '../actions/actionTypes'

const initialState = {
    token: null,
    userId: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_TOKEN:
            return {
                ...state,
                token: action.token,
                userId: action.userId
            };
        default:
            return state;
    }
}

export default reducer;