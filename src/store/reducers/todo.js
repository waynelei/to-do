import * as actionTypes from '../actions/actionTypes'

const initialState = {
    todos: [],
    loading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_TODOS:
            return {
                ...state,
                todos: action.todos
            }
        case actionTypes.SET_TODO_LOADING:
            return {
                ...state,
                loading: action.loading
            }

        default:
            return state;
    }
}



export default reducer;