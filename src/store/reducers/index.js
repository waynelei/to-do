import { combineReducers } from 'redux';

import user from './user';
import todo from './todo';

const rootReducer = combineReducers ({
    user: user,
    todo: todo
});

export default rootReducer;